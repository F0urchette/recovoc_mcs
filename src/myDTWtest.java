import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;

import fr.enseeiht.danck.voice_analyzer.DTWHelper;
import fr.enseeiht.danck.voice_analyzer.Field;
import fr.enseeiht.danck.voice_analyzer.defaults.DTWHelperDefault;

public class myDTWtest {

//		protected static final int MFCCLength = 13;
		protected static final String base = "/test_res/audio/";
		private myDTW myDTWHelper = new myDTW();
		private DTWHelper DTWHelperDefault= new DTWHelperDefault();
		protected int nbTotal;
		
		static int FieldLength(String fileName) throws IOException {
			int counter= 0;
			File file = new File(System.getProperty("user.dir") + fileName);
            for (String line : Files.readAllLines(file.toPath(), Charset.defaultCharset())) {
            	counter++;
            }
            return 2*Math.floorDiv(counter, 512);
		}

		public void compare2Mots(String mot1, String mot2) throws IOException, InterruptedException {
		    
	        Field alphaField = this.myDTWHelper.createField(base + mot1, FieldLength(base + mot1));
//	        float mydistanceAlphaAlpha= myDTWHelper.DTWDistance(alphaField, alphaField);
//	        float distanceAlphaAlphadefault= DTWHelperDefault.DTWDistance(alphaField, alphaField);
	        
//	        System.out.println("myDTW - valeur distance Alpha-Alpha calculee : "+mydistanceAlphaAlpha);
//	        System.out.println("DTWHelperDefault - valeur distance Alpha-Alpha calculee : "+distanceAlphaAlphadefault);
	        
	        // Etape 1. Lecture de Bravo
	        Field bravoField= this.myDTWHelper.createField(base + mot2, FieldLength(base + mot2));
	        float mydistanceAlphaBravo= myDTWHelper.DTWDistance(alphaField, bravoField);
	        float distanceAlphaBravodefault= DTWHelperDefault.DTWDistance(alphaField, bravoField);
	        
	        System.out.println("myDTW 			| distance " + mot1.substring(0, mot1.length() - 4) + " - " + mot2.substring(0, mot2.length() - 4) + " : " + mydistanceAlphaBravo);
	        System.out.println("DTWHelperDefault 	| distance " + mot1.substring(0, mot1.length() - 4) + " - " + mot2.substring(0, mot2.length() - 4) + " : " + distanceAlphaBravodefault
	        		+ "\n-----------------------------");
		}
		
		
		
		
		
		
		private String parseOrder(String filename) {
			return filename.split("[_.]")[1];
		}
		
		protected float tauxErreur(Object[][] data) {
			int nbErreur = 0;
			this.nbTotal = 0;
			
			for (int i = 0; i < data.length; i++) {
				for (int j = 1; j < data.length + 1; j++) {
					this.nbTotal += (int) data[i][j];
					if (i != j - 1)
						nbErreur += (int) data[i][j];
				}
			}
			
			System.out.println(nbTotal);
			
			return (float) nbErreur / this.nbTotal;
		}
		
		public void matrice() {
			Map<String, Map<String, Integer>> mat = new TreeMap<>();
			Map<String, Field> fieldsRef = new TreeMap<>();
			
			
			File dir = new File("."+base+"/reference");
			String[] listRef = dir.list(new FilenameFilter() {
			    @Override
			    public boolean accept(File dir, String name) {
			        return name.toLowerCase().matches(".*\\.csv");
			    }
			});
			
			Field field;
			String order;
			
			for (String e : listRef) { // On construit le tableau des Fields des references et la matrice de confusion
				try {
					field = this.myDTWHelper.createField(base + "reference/" + e, FieldLength(base + "reference/" + e));
					order = this.parseOrder(e);
					fieldsRef.put(order, field);
					
					mat.put(order, new TreeMap<String, Integer>());
					
					for(String f : listRef) {
						mat.get(order).put( this.parseOrder(f), 0);
					}
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			
			File folder = new File("."+base+"/tests/");
			File[] listOfFiles = folder.listFiles( new FilenameFilter() {
				
				@Override
				public boolean accept(File dir, String name) {
					// TODO Auto-generated method stub
					return name.endsWith(".csv");
//					return name.startsWith("PBM");
				}
			});
			Pair<Integer, String>[] dtwDistances;
			File file;
			int min;
			String ordreDevine;
			
			for (int i = 0; i < listOfFiles.length; i++) { // On itere sur les fichiers � tester
				dtwDistances = new Pair[listRef.length];
			
				file = listOfFiles[i];
			    if (file.isFile()) {
			    	try {
						field = this.myDTWHelper.createField(base + "tests/" + file.getName(), FieldLength(base + "tests/" + file.getName()));
						order = this.parseOrder(file.getName());
//						System.out.println("Ordre a tester : " + order);
				        int k = 0;
				        
				        for (String key : fieldsRef.keySet()) {
				        	dtwDistances[k] = new Pair<Integer, String>((int) this.myDTWHelper.DTWDistance(fieldsRef.get(key), field), key);
//				        	System.out.println("Ref a comparer : " + key);
//				        	System.out.println(dtwDistances[k]);
				        	k++;
				        }
				        	
				        
				        min = 0;
				        
				        for (int j = 1; j < dtwDistances.length; j++) {
							if (dtwDistances[min].getX() > dtwDistances[j].getX())
								min = j;
				        }
				        
				        ordreDevine = dtwDistances[min].getY();
				       
				        mat.get(order).put(ordreDevine, mat.get(order).get(ordreDevine) + 1);
				        
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			        
			    	
			    }
			    
			}
			
			
			class AfficheMat extends JFrame {
				/**
				 * 
				 */
				private static final long serialVersionUID = 4378788530852520935L;
				private JTable table;
				private Object[] title=  mat.keySet().toArray();  //entete du tableau
				private Object[] title2;
				private Object[][] data = new Object[listRef.length][listRef.length+1];
				private JScrollPane scroll;
				private JTextField jtf;
				
				public AfficheMat() {
					for (int i = 0; i < listRef.length; i++) {
						for (int j = 0; j < listRef.length + 1; j++) {
							if (j == 0)
								data[i][j] = title[i];
							else {
								data[i][j] = mat.get(title[i]).get(title[j - 1]);
							}
						}
					}
					
					this.title2 = new Object[this.title.length + 1];
					this.title2[0] = "";
					for (int i = 0; i < title.length; i++) {
						this.title2[i + 1] = this.title[i];
					}
					
					
					
					this.setTitle("Matrice de confusion");
					this.setLocationRelativeTo(null);
					this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			 
					table = new JTable(data, title2);   //initialiser le tableau
					scroll = new JScrollPane(table); 
					jtf = new JTextField("Taux d'erreur : " + 100 * tauxErreur(data) + " %	pour " + nbTotal + " valeurs testees.");
					
					JPanel jpanel = new JPanel(new BorderLayout());
					this.setSize(new Dimension(1500, 400));
					jpanel.add(scroll, BorderLayout.CENTER);
					jpanel.add(jtf, BorderLayout.SOUTH);
					jtf.setEditable(false);
					jtf.setHorizontalAlignment(JTextField.CENTER);
					
					DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
					centerRenderer.setHorizontalAlignment( JLabel.CENTER );
					table.setDefaultRenderer(Object.class, centerRenderer);
					
			 
					this.getContentPane().add(jpanel);
					this.setVisible(true);
				}
			}
			
			new AfficheMat();
			
			System.out.println(mat);
			
		}
		
		
		public static void main(String[] args) throws IOException, InterruptedException {
			myDTWtest test = new myDTWtest();
//			test.compare2Mots("Charlie.csv", "Alpha.csv");
//			test.compare2Mots("Charlie.csv", "Delta.csv");
//			test.compare2Mots("F04_arretetoi.csv", "F03_arretetoi.csv");
//			test.compare2Mots("F04_arretetoi.csv", "F01_tournegauche.csv");
			test.matrice();
			
			
		}

}
