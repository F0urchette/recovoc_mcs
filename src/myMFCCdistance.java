import fr.enseeiht.danck.voice_analyzer.MFCC;
import fr.enseeiht.danck.voice_analyzer.MFCCHelper;

public class myMFCCdistance extends MFCCHelper {

	@Override
	public float distance(MFCC mfcc1, MFCC mfcc2) {
		float sum = 0;
		int length = mfcc1.getLength();									// We get the number of coefficients
		
		for (int i = 0; i < length; i++) {								// We calculate the distance between mfcc1 and mfcc2
			sum += Math.pow(mfcc1.getCoef(i) - mfcc2.getCoef(i), 2);
		}
		
		return (float) Math.sqrt(sum);
	}

	@Override
	public float norm(MFCC mfcc) {
		return mfcc.getCoef(0);
	}

	@Override
	public MFCC unnoise(MFCC mfcc, MFCC noise) {
		// supprime le bruit de la MFCC passee en parametre
		// soustrait chaque coef du bruit a chaque coef du la MFCC 
		// passee en parametre
		
		int length = mfcc.getLength();								// We get the number of coefficients
		float newMfccCoeffs[] = new float[length];					// Array for the new coeffs
		
		for (int i = 0; i < length; i++) {							// We substract each coeff of the MFCC with the corresponding coeff of the noise
			newMfccCoeffs[i] = mfcc.getCoef(i) - noise.getCoef(i);	
		}
		
		return new MFCC(newMfccCoeffs, mfcc.getSignal());			// We return the new MFCC with the new coeffs
	}

}
