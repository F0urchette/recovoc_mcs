import java.io.IOException;

import fr.enseeiht.danck.voice_analyzer.DTWHelper;
import fr.enseeiht.danck.voice_analyzer.Extractor;
import fr.enseeiht.danck.voice_analyzer.Field;
import fr.enseeiht.danck.voice_analyzer.MFCC;
import fr.enseeiht.danck.voice_analyzer.WindowMaker;

public class myDTW extends DTWHelper {
	private Extractor extractor = Extractor.getExtractor();
	WindowMaker windowMaker;

	@Override
	public float DTWDistance(Field unknown, Field known) {
		// Methode qui calcule le score de la DTW 
		// entre 2 ensembles de MFCC
		
		int w0 = 1;
		int w2 = 1;
		int w1 = 2;
		int I = unknown.getLength();
		int J = known.getLength();
		float g[][] = new float[I][J];
		g[0][0] = 0;
		myMFCCdistance myDistance = new myMFCCdistance();
		float d;
		
		for (int j = 1; j < J; j++) {
			g[0][j] = Float.MAX_VALUE;
		}
		for (int i = 1; i < I; i++) {
			g[i][0] = Float.MAX_VALUE;
			for (int j = 1; j < J; j++) {
				d = myDistance.distance(unknown.getMFCC(i), known.getMFCC(j));
				g[i][j] = Math.min(g[i][j - 1] + w2 * d, Math.min(g[i - 1][j] + w0 * d, g[i - 1][j - 1] + w1 * d));
			}
		}
		
		return (g[I - 1][J - 1] / (I + J));
	}
	
	public Field createField(String files, int length) throws IOException, InterruptedException {
		this.windowMaker = new MultipleFileWindowMaker(files);
	    
	    // Etape 2. Recuperation des MFCC du mot Alpha
	    MFCC[] mfccsAlpha = new MFCC[length];
        for (int i = 0; i < mfccsAlpha.length; i++) {
            mfccsAlpha[i] = extractor.nextMFCC(windowMaker);
        }
        
        // Etape 3. Construction du Field (ensemble de MFCC) de alpha
        return new Field(mfccsAlpha);
	}

}
